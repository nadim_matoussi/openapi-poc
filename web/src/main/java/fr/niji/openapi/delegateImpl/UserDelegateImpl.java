package fr.niji.openapi.delegateImpl;

import fr.niji.openapi.UserApiDelegate;
import fr.niji.openapi.exceptions.ResourceNotFoundException;
import fr.niji.openapi.exceptions.UserNotFoundException;
import fr.niji.openapi.model.User;
import fr.niji.openapi.services.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserDelegateImpl implements UserApiDelegate {
    private final UserService userService;

    public UserDelegateImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public ResponseEntity<Void> createUser(User body) {
        this.userService.createUser(body);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<User> getUserById(Integer id) {
        try {
            return ResponseEntity.ok(this.userService.findUser(id));
        } catch (UserNotFoundException e) {
            throw new ResourceNotFoundException(e.getMessage());
        }
    }

    @Override
    public ResponseEntity<Void> deleteUser(Integer id) {
        try {
            this.userService.delete(id);
            return ResponseEntity.ok().build();
        } catch (UserNotFoundException e) {
            throw new ResourceNotFoundException(e.getMessage());
        }
    }

    @Override
    public ResponseEntity<User> updateUser(User body) {
        try {
            this.userService.update(body);
            return ResponseEntity.ok().build();
        } catch (UserNotFoundException e) {
            throw new ResourceNotFoundException(e.getMessage());
        }
    }

    @Override
    public ResponseEntity<List<User>> getAllUsers() {
        return ResponseEntity.ok(this.userService.findAll());
    }
}



