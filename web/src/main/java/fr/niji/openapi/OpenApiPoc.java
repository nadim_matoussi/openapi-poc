package fr.niji.openapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpenApiPoc {

    public static void main(String[] args) {
        SpringApplication.run(OpenApiPoc.class, args);
    }

}
