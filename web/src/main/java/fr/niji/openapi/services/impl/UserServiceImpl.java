package fr.niji.openapi.services.impl;

import fr.niji.openapi.exceptions.UserNotFoundException;
import fr.niji.openapi.model.User;
import fr.niji.openapi.repositories.UserRepository;
import fr.niji.openapi.services.UserService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void createUser(User user) {
        this.userRepository.createUser(user);
    }

    @Override
    public User findUser(Integer id) throws UserNotFoundException {
        return this.userRepository.findUser(id);
    }

    @Override
    public List<User> findAll() {
        return this.userRepository.findAll();
    }

    @Override
    public void delete(Integer id) throws UserNotFoundException {
        this.userRepository.delete(id);
    }

    @Override
    public void update(User user) throws UserNotFoundException {
        this.userRepository.update(user);
    }
}
