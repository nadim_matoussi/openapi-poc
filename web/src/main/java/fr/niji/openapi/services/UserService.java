package fr.niji.openapi.services;

import fr.niji.openapi.exceptions.UserNotFoundException;
import fr.niji.openapi.model.User;

import java.util.List;

public interface UserService {
    void createUser(User user);
    User findUser(Integer id) throws UserNotFoundException;
    List<User> findAll();

    void delete(Integer id) throws UserNotFoundException;

    void update(User user) throws UserNotFoundException;
}
