package fr.niji.openapi.advices;

import fr.niji.openapi.exceptions.ResourceNotFoundException;
import fr.niji.openapi.model.ResponseError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice(basePackages = {"fr.niji.openapi.controllers"})
public class UserControllerAdvice {
    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ResponseError> handleException(Exception ex) {

        var responseError = new ResponseError();
        responseError.setCode(1);
        responseError.setMessage(ex.getMessage());
        return new ResponseEntity<>(responseError, HttpStatus.NOT_FOUND);
    }
}
