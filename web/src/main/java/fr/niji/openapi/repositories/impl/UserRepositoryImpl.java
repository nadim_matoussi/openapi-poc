package fr.niji.openapi.repositories.impl;

import fr.niji.openapi.exceptions.UserNotFoundException;
import fr.niji.openapi.model.User;
import fr.niji.openapi.repositories.UserRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl implements UserRepository {
    private final List<User> users = new ArrayList<>();
    @Override
    public void createUser(User user) {
        this.users.add(user);
    }

    @Override
    public User findUser(Integer id) throws UserNotFoundException {
        return this.users.stream()
                .filter(user -> user.getId().equals(id))
                .findFirst()
                .orElseThrow(() -> new UserNotFoundException("User Not found"));

    }
    @Override
    public List<User> findAll() {
        return new ArrayList<>(users);
    }

    @Override
    public void delete(Integer id) throws UserNotFoundException {
        var foundUser = users.stream()
                .filter(user -> user.getId().equals(id))
                .findFirst();

        if (foundUser.isPresent()) {
            users.remove(foundUser.get());
        } else {
            throw new UserNotFoundException("User not found");
        }
    }

    @Override
    public void update(User user) throws UserNotFoundException {
        int index = -1;
        for (int i = 0; i < users.size(); i++) {
            User existingUser = users.get(i);
            if (existingUser.getFirstName().equals(user.getFirstName())) {
                index = i;
                break;
            }
        }

        if (index != -1) {
            users.set(index, user);
        } else {
            throw new UserNotFoundException("User not found");
        }
    }
}
