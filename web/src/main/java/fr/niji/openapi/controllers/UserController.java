package fr.niji.openapi.controllers;


import fr.niji.openapi.UserApi;
import fr.niji.openapi.UserApiDelegate;
import fr.niji.openapi.model.User;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController implements UserApi {
    private final UserApiDelegate userApiDelegate;

    public UserController(UserApiDelegate userService) {
        this.userApiDelegate = userService;
    }

    @Override
    public ResponseEntity<User> getUserById(Integer id) {
        return this.userApiDelegate.getUserById(id);
    }

    @Override
    public ResponseEntity<Void> createUser(User body) {
        return this.userApiDelegate.createUser(body);
    }

    @Override
    public ResponseEntity<Void> deleteUser(Integer id) {
        return this.userApiDelegate.deleteUser(id);
    }

    @Override
    public ResponseEntity<User> updateUser(User body) {
        return this.userApiDelegate.updateUser(body);
    }

    @Override
    public ResponseEntity<List<User>> getAllUsers() {
        return this.userApiDelegate.getAllUsers();
    }
}
