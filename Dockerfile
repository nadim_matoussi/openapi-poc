# Use a base image with Maven and Java
FROM maven:3.8.4-openjdk-11-slim AS build

# Set the working directory inside the container
WORKDIR /app

# Copy the Maven project file
COPY pom.xml .
# Copy the source code
COPY . .

# Build the application
RUN mvn clean install

# Build the final image with the compiled application
FROM openjdk:11-slim

# Set the working directory inside the container
WORKDIR /app

# Copy the compiled jar from the build stage
COPY --from=build /app/web/target/*.jar app.jar

# Expose port 8080
EXPOSE 8080

# Set the entry point to run the jar file
ENTRYPOINT ["java", "-jar", "app.jar"]
